import pyglet
from pyglet.window import key

window = pyglet.window.Window(
    width=800,
    height=600,
    caption="Meu primeiro porquinho")
x = window.width // 2
y = window.height // 2
#bola = pyglet.shapes.Circle(w,h,100)
bola = pyglet.resource.image('Basketball.png')

@window.event
def on_draw():
    window.clear()
    bola.blit(x - bola.width//2, y - bola.height//2)

@pyglet.clock.schedule
def update(dt):
    global x, y
    x += 2
    y += 2
    if x > window.width: x -= window.width
    if x < 0: x += window.width
    if y > window.height: y -= window.height
    if y < 0: y += window.height

@window.event
def on_key_press(symbol, modifier):
    global x, y
    if (symbol == key.UP):
        y += 10
    elif (symbol == key.DOWN):
        y -= 10
    elif (symbol == key.LEFT):
        x -= 10
    elif (symbol == key.RIGHT):
        x += 10
    
    
pyglet.app.run()
